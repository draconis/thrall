#!/bin/bash

# Définition du répertoire de travail
WKDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/.." && pwd )"

echo $WKDIR

source $WKDIR/.env

#création des volumes persistants
mkdir -p $WKDIR/$DATA_DIR/{baikal/db,baikal/config,baikal/data,bitwarden/data,shaarli/data,shaarli/cache,koken/db,koken/app,cachet/postgres,cachet/app,traefik}

# Création du réseau web
docker network create web


