// Username displayed in greeting.
var userName = "";

// Default Searchengine
// Available: Google DuckDuckGo Bing Yahoo
var searchEngine = "Google";

// Cards
var cards = [
	{
		name: "Serveurs",
		bookmarks: {
			"Dalaran": "https://163.172.48.42:8006/",
			"voljin": "aaa",
			"thrall": "aaa",
			"photos": "aaa",
			"iRedMail": "https://mail.delort.email/iredadmin"
		}
	},
	{
		name: "Tech",
		bookmarks: {
			"gtilab": "https://framagit.org/users/sign_in?redirect_to_referer=yes",
			"shaarli": "https://shaarli.draconis.me",
			"jellyfin": "aaa",
			"sonarr": "aaa",
			"radarr": "aaa"
		}
	},
	{
		name: "Work",
		bookmarks: {
			"arena": "http://sconetab.ag.in.ac-montpellier.fr/arena/pages/accueill.jsf",
			"accolad": "https://accolad.ac-montpellier.fr/user/login?destination=/home",
			"ArS": "https://aiderestaurationscolaire.herault.fr/identity/connect",
			"amazon": "aaa",
			"dummysite": "aaa"
		}
	},
	{
		name: "Social",
		bookmarks: {
			"courriel": "https://mail.delort.email/",
			"gmail": "https://gmail.com/",
			"outlook": "https://outlook.live.com/owa/",
			"mastodon": "https://mstdn.io",
			"hacker": "https://www.journalduhacker.net/"
		}
	}
]
