// Username displayed in greeting.
var userName = "";

// Default Searchengine
// Available: Google DuckDuckGo Bing Yahoo
var searchEngine = "Google";

// Cards
var cards = [
	{
		name: "Serveurs",
		bookmarks: {
			"Dalaran": "https://163.172.48.42:8006/",
			"voljin": "aaa",
			"thrall": "aaa",
			"iRedMail": "https://mail.delort.email/iredadmin"
		}
	},
	{
		name: "Services",
		bookmarks: {
			"gtilab": "https://framagit.org/users/sign_in?redirect_to_referer=yes",
			"shaarli": "https://shaarli.draconis.me",
			"jellyfin": "aaa",
			"iCloud": "https://www.icloud.com/",
			"ovh": "https://www.ovh.com/auth/?action=gotomanager&from=https://www.ovh.com/fr/&ovhSubsidiary=fr"
		}
	},
	{
		name: "Liens",
		bookmarks: {
			"arena": "http://sconetab.ag.in.ac-montpellier.fr/arena/pages/accueill.jsf",
			"accolad": "https://accolad.ac-montpellier.fr/user/login?destination=/home",
			"ArS": "https://aiderestaurationscolaire.herault.fr/identity/connect",
			"JdH": "https://www.journalduhacker.net/"
		}
	},
	{
		name: "Social",
		bookmarks: {
			"courriel": "https://mail.delort.email/",
			"gmail": "https://gmail.com/",
			"outlook": "https://outlook.live.com/owa/",
			"mastodon": "https://mstdn.io"
		}
	}
]
